package com.atlassian.maven.plugins.stash;

import com.atlassian.maven.plugins.amps.osgi.GenerateObrArtifactMojo;

import org.apache.maven.plugins.annotations.Mojo;

/**
 * @since 3.10
 */
@Mojo(name = "generate-obr-artifact")
public class StashGenerateObrArtifactMojo extends GenerateObrArtifactMojo
{
}
